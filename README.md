# 'whisper_ros' Package

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**
*   [Setup](#Setup)
*   [Launch](#launch)


## Setup
- sudo apt install portaudio19-dev
- sudo apt install pulseaudio
- pip3 install pyaudio
- pip3 install SpeechRecognition
- python3 -m pip install git+https://github.com/openai/whisper.git soundfile 

## Launch
- python whisper_ros.py

