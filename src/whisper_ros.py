#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
from std_msgs.msg import String
import speech_recognition as sr #[*] Load the module for speech recognition.


class Recognition(): #[*] To use the Recognition class as a node, it inherits from the Node class.
    def __init__(self):

        rospy.loginfo(f'Launch a node...') #[*] The logger is used to indicate that a node has been activated.

        self.init_rec = sr.Recognizer() #[*] Initializes the speech recognizer.

        self.publisher = rospy.Publisher('/speech', String, queue_size=1) #[*] Initialize the Publisher to send the recognized voice to the topic speech.

    def recognition(self):
        while not rospy.is_shutdown():
            with sr.Microphone() as source: #[*] Enables the handling of sound acquisition data.
                text = ''

                audio_data = self.init_rec.record(source, duration=5) #[*] Enables 5 seconds of sound acquisition data to be retrieved.
                    
                rospy.loginfo(f'Speech Recognition...')

                try:
                    text = self.init_rec.recognize_whisper(audio_data, model="medium", language="english") #[*] Send the sound data to Whisper and receive the results of speech recognition.
        
                except sr.UnknownValueError:
                    pass

                
                if text != '': #[*] When a speech recognition result is obtained, it is sent to the speech topic.
                    msg = String()
                    msg.data = text
                    rospy.loginfo(
                        f'Recognized voice "{text}" is published to topic name /speech')

                    self.publisher.publish(msg)

if __name__ == '__main__':
    rospy.init_node('whisper_ros')
    recognition_node = Recognition() #[*] Initializes the speech recognition node.
    recognition_node.recognition()
    rospy.spin() #[*] Runs a speech recognition node.